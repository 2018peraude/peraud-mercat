from add_seed_to_universe import *
from create_seed import *
from generate_universe import *
from pytest import *
import numpy as np

def test_add_seed_to_universe():
    seed = create_seed("r_pentomino")
    universe = generate_universe((6,6))
    assert (add_seed_to_universe(seed, universe,1,1)== np.array([[0,0, 0, 0, 0, 0],[0, 0, 1, 1, 0, 0],[0, 1, 1, 0, 0, 0],[0 ,0, 0, 0, 0, 0],[0 ,0, 0, 0, 0, 0],[0 ,0, 0, 0, 0, 0]])).all()

