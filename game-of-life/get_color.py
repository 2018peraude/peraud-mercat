import numpy as np

bibli_of_color={'Black': (0,0,0),'White':(255,255,255),'Red':(255,0,0),'Blue':(0,0,255),'Green':(0,255,0),'Purple':(132,122,191)}

def get_color(couleur):
    return bibli_of_color[couleur]

#print(get_color('Black'))
