from amination import*
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("size1", help="Choisissez la taille verticale de l'univers",type=int)
parser.add_argument("size2", help="Choisissez la taille horizontale de l'univers",type=int)
parser.add_argument("type_seed", help="Choisissez votre graine",type=str)
parser.add_argument("seed_position1", help="Choisissez la position verticale du coin en haut à gauche de votre graine dans l'univers",type=int)
parser.add_argument("seed_position2", help="Choisissez la position horizontale du coin en haut à gauche de votre graine dans l'univers",type=int)
parser.add_argument("cmap", help="Choisissez la couleur d'affichage",type=str)
parser.add_argument("iteration", help="Choisissez le nombre d'évolutions que votre univers va subir",type=int)
parser.add_argument("speed", help="Choisissez la rapidité des évolutions en ms",type=int)
parser.add_argument("save", help="Voulez vous sauvegarder votre évolution? oui: écrivez True, non: écrivez False",type=bool)
args = parser.parse_args()

#print(animation((args.size1,args.size2),args.type_seed,(args.seed_position1,args.seed_position2),cmap=args.cmap,iteration=args.iteration,speed=args.speed,save=args.save)
