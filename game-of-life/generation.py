from survival import *

def generation(universe):
    #met à jour l'univers sur l'espace entier
    x,y=np.shape(universe)
    univ_generation=np.zeros((x,y)) #cree un univers vide pret à acceuillir les évolutions
    for i in range(x):
        for j in range(y):
            univ_generation[i][j]=survival(universe,i,j)[i][j] #rempli l'univers avec les évolutions
    return univ_generation

