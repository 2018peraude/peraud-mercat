from create_seed import *
from pytest import *
from numpy import *

def test_create_seed():
    seed=create_seed("r_pentomino")
    assert (seed==np.array([[0, 1, 1], [1, 1, 0], [0, 1, 0]])).all()
