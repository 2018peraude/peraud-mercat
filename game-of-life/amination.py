from simple_animation import *
from generate_universe import *
from create_seed import *
from add_seed_to_universe import *

def animation(size,type_seed,seed_position,cmap,iteration=30,speed=300,save=False):
    #permet de cree l'univers selon un choix de graine et de taille d'univers
    # et de lancer l'animation selon des parametres choisis
    empty_universe=generate_universe(size)
    seed=create_seed(type_seed)
    universe=add_seed_to_universe(seed,empty_universe,seed_position[0],seed_position[1])   #cree l'univers de départ
    return simple_animation(universe,cmap,iteration,speed)                                      #réalise l'animation

#print(animation((6,6),"beacon",(1,1),iteration=10,speed=300,save=False))
