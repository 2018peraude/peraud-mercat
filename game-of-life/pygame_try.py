# #afficher image
# import pygame
#
# pygame.init()
#
# ecran = pygame.display.set_mode((600, 600))
# image = pygame.image.load("162px-Game_of_life_infinite1.svg.png").convert_alpha()
# ecran.blit(image, (10,10))
#
# continuer = True
#
# while continuer:
#     for event in pygame.event.get():
#         if event.type == pygame.KEYDOWN:
#             continuer = False
#     pygame.display.flip()
#
#
#
# pygame.quit()

##quitter avec une seule touche
# import pygame
#
# pygame.init()
#
# ecran = pygame.display.set_mode((640, 480))
#
# continuer = True
#
# while continuer:
#     for event in pygame.event.get():
#         if event.type == pygame.KEYDOWN:
#             if event.key == pygame.K_e:
#                 continuer = False
#
# pygame.quit()

# #faire bouger image avec sourie
# import pygame
#
# pygame.init()
#
# ecran = pygame.display.set_mode((640, 480))
# clem = pygame.image.load("162px-Game_of_life_infinite1.svg.png").convert_alpha()
# pos_clem = (0, 0)
#
# continuer = True
#
# while continuer:
#     pygame.draw.rect(ecran, (255, 255, 255), (0, 0, 640, 480))
#     for event in pygame.event.get():
#         if event.type == pygame.MOUSEMOTION:
#             pos_clem = event.pos
#         if event.type == pygame.QUIT:
#             continuer = False
#     ecran.blit(clem, pos_clem)
#     pygame.display.flip()
# pygame.quit()

# #dessiner au relachement de la sourie
# import pygame
#
# pygame.init()
#
# ecran = pygame.display.set_mode((640, 480))
#
# largeur = 10
# hauteur = 10
# couleur = (20, 180, 20)
# continuer = True
#
# while continuer:
#     for event in pygame.event.get():
#         if event.type == pygame.MOUSEBUTTONUP:
#             x, y = event.pos
#             pygame.draw.rect(ecran, couleur, (x, y, largeur, hauteur))
#         if event.type == pygame.QUIT:
#             continuer = False
#     pygame.display.flip()
#
# pygame.quit()

# #gerer les evenements
# for event in my_event_listenner():
#     if event == (MOUSEBUTTONDOWN, 1):
#         # si on a un clic souris enfoncé avec le bouton n°1 ...
#     if event == (KEYDOWN, K_t):
#         # si on a une key down avec la touche t ...

## jeu
# import pygame
# from pygame import locals as const
# from game import Game
#
#
# def main():
#     print("Appuyez sur n'importe quelle touche pour lancer la partie !")
#
#     pygame.init()
#
#     ecran = pygame.display.set_mode((640, 480))
#     fond = pygame.image.load("162px-Game_of_life_infinite1.svg.png").convert_alpha()
#
#     continuer = True
#     jeu = Game(ecran)# Game() est une class qui va se charger ... du jeu :)
#
#     while continuer:
#         for event in pygame.event.get():
#             if event.type == const.QUIT or (event.type == const.KEYDOWN and event.key == const.K_ESCAPE):
#                 # de manière à pouvoir quitter le menu avec echap ou la croix
#                 continuer = 0
#             if event.type == const.KEYDOWN:
#                 # start() sera une méthode de la class Game(), et s'occupera de lancer le jeu
#                 jeu.start()
#
#         ecran.blit(fond, (0, 0))
#
#         pygame.display.flip()
#
#     pygame.quit()
#
#
# if __name__ == '__main__':
#     main()
#
# import pygame
# from constantes import *
#
#
# class Raquette:
#     def __init__(self, ecran: pygame.Surface):
#         self.ecran = ecran
#         self.ecran_large = self.ecran.get_width()  # ca vous nous servir pour centrer la raquette et regarder
#                                                    # si la raquette sort de l'écran ou non
#         self.ecran_haut = self.ecran.get_height()  # idem
#         self.image = pygame.image.load("162px-Game_of_life_infinite1.svg.png").convert_alpha()
#         self.pos = [20, (self.ecran_haut - self.image.get_height()) // 2]  # on centre notre raquette à droite
#
#     def render(self):
#         self.ecran.blit(self.image, self.pos)
#
#     def move(self, dir: int=RIEN):
#         # on test toutes les collisions possibles
#         # et voici l'utilité de nos constantes !
#         if dir == HAUT:
#             if self.pos[1] - VITESSE_RAQUETTE >= 0:
#                 self.pos[1] -= VITESSE_RAQUETTE
#             else:
#                 self.pos[1] = 0
#         elif dir == BAS:
#             if self.pos[1] + VITESSE_RAQUETTE <= self.ecran_haut - self.image.get_height():
#                 self.pos[1] += VITESSE_RAQUETTE
#             else:
#                 self.pos[1] = self.ecran_haut - self.image.get_height()
#
#     def collide_with_me(self, pos_objet: tuple, taille_objet: tuple):
#         # utile pour savoir si la balle collide avec la raquette
#         if self.pos[0] <= pos_objet[0] <= self.pos[0] + self.image.get_width() and \
#                 self.pos[1] <= pos_objet[1] <= self.pos[1] + self.image.get_height():
#             # le coté gauche de la balle est dans la raquette
#             return True
#         elif self.pos[0] <= pos_objet[0] + taille_objet[0] <= self.pos[0] + self.image.get_width() and \
#                 self.pos[1] <= pos_objet[1] + taille_objet[1] <= self.pos[1] + self.image.get_height():
#             # le coté droit de la balle est dans la raquette
#             return True
#         # enfin, on a pas eu de collision, donc on return False
#         return False

