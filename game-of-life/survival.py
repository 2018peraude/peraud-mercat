import numpy as np

def compte_voisins(universe,x,y):
    #compte les voisins vivants de la cellule
    n,p=np.shape(universe)
    voisins=0
    borneinf_i,bornesup_i=-1,2    #permet d'eviter de compter deux fois une même cellule si l'univers est trop petit
    borneinf_j,bornesup_j=-1,2
    if n<=2:
        borneinf_i=0
    if n==1:
        bornesup_i=1
    if p<=2:
        borneinf_j=0
    if p==1:
        bornesup_j=1
    for i in range(borneinf_i,bornesup_i):
        for j in range(borneinf_j,bornesup_j):
            if i!=0 or j!=0:
                voisins+=universe[(x+i)%n][(y+j)%p] #permet de prendre en compte la geometrie de tore
    return voisins


def survival(universe,x,y):
    #donne l'evolution d'une case choisie de l'univers
    new_universe=np.copy(universe) #evite les modifications de l'univers de départ
    voisins=compte_voisins(new_universe,x,y)
    vivante=new_universe[x][y]
    if vivante:
        if voisins!=2 and voisins!=3:
            new_universe[x][y]=0       #met a jour la cellule
    else:
        if voisins==3:
            new_universe[x][y]=1
    return new_universe
