from tkinter import *
import tkinter.ttk as ttk
import random as rd
from amination import *

def lancer_fenetre():
    def lancer_simu():
        size=(int(var_hauteur.get()),int(var_largeur.get()))
        type_seed=graine.get()
        (x,y)=(int(var_abscisse.get()),int(var_ordonnee.get()))
        Cmap=cmap.get()
        nb_iterations=int(var_iterations.get())
        save=var_case.get()
        if x==-1:
            x=rd.randint(0,size[0])
        if y==-1:
            y=rd.randint(0,size[1])
        print(animation(size,type_seed,(y,x),Cmap,nb_iterations,speed=300,save=False))
    # On crée une fenêtre, racine de notre interface
    fenetre = Tk()

    # Label
    champ_label = Label(fenetre, text="Rentrez les paramètres d'initialisation du jeu",font= ('Times', '24', 'bold'))
    champ_label.pack()

    frame=Frame(fenetre)
    frame.pack()

    #taille de la grille

    var_largeur=StringVar(frame)
    text_largeur= Label(frame, text= "largeur de la grille",font=('Times','16'))
    text_largeur.grid(row=0,column=0)
    largeur = Entry (frame,textvariable=var_largeur)
    largeur.grid(row=0,column=2)

    var_hauteur=StringVar(frame)
    text_hauteur= Label(frame, text= "hauteur de la grille",font=('Times','16'))
    text_hauteur.grid(row=1,column=0)
    hauteur = Entry (frame,textvariable=var_hauteur)
    hauteur.grid(row=1,column=2)

    #type de graine
    text_graine= Label(frame, text= "type de graine",font=('Times','16'))
    text_graine.grid(row=2,column=0)
    graine = ttk.Combobox(frame,values=["boat","r_pentomino","beacon","acorn","block_switch_engine","infinite","glider","pentadecathlon","blinker","toad"])
    graine.grid(row=2,column=2)

    #Place de la graine

    var_abscisse=StringVar(frame)
    text_abscisse= Label(frame, text= "abscisse de la graine (-1 pour aléatoire)",font=('Times','16'))
    text_abscisse.grid(row=3,column=0)
    abscisse = Entry (frame,textvariable=var_abscisse)
    abscisse.grid(row=3,column=2)
    var_abscisse.set("-1")

    var_ordonnee=StringVar(frame)
    text_ordonnee= Label(frame, text= "ordonnee de la graine (-1 pour aléatoire)",font=('Times','16'))
    text_ordonnee.grid(row=4,column=0)
    ordonnee = Entry (frame,textvariable=var_ordonnee)
    ordonnee.grid(row=4,column=2)
    var_ordonnee.set("-1")

    #nombre d'itérations
    var_iterations=StringVar(frame)
    text_iterations= Label(frame, text= "nombre d'itérations (-1 pour un infinité)",font=('Times','16'))
    text_iterations.grid(row=5,column=0)
    iterations = Entry (frame,textvariable=var_iterations)
    iterations.grid(row=5,column=2)
    var_iterations.set("-1")

    #cmap
    text_cmap= Label(frame, text= "cmap",font=('Times','16'))
    text_cmap.grid(row=6,column=0)
    cmap = ttk.Combobox(frame,values=["Greys","Purples","Blues","Greens","BuPu","cool","autumn"])
    cmap.grid(row=6,column=2)
    cmap.current(0)

    # Sauvegarder
    var_case = BooleanVar()
    case = Checkbutton(fenetre, text="Sauvegarder", variable=var_case,font=('Times','16'))
    case.pack()

    #Bouton Lancer
    bouton_lancer=Button(fenetre,text="Lancer la simulation",command=lancer_simu,font=('Times','16'))
    bouton_lancer.pack()

    # On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
    fenetre.mainloop()

#lancer l'animation
if __name__=="__main__":
        lancer_fenetre()
