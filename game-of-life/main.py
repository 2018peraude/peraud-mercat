from amination import *

def main():
    size1=int(input("Choisissez la taille verticale de l'univers: "))
    size2=int(input("Choisissez la taille horizontale de l'univers: "))
    type_seed=input("Choisissez votre graine: ")
    seed_position1=int(input("Choisissez la position verticale du coin en haut à gauche de votre graine dans l'univers: "))
    seed_position2=int(input("Choisissez la position horizontale du coin en haut à gauche de votre graine dans l'univers: "))
    cmap=input("Choisissez la couleur d'affichage: ")
    iteration=int(input("Choisissez le nombre d'évolutions que votre univers va subir: "))
    speed=int(input("Choisissez la rapidité des évolutions en ms: "))
    save=input("Voulez vous sauvegarder votre évolution? oui: écrivez True, non: écrivez False: ")
    return animation((size1,size2),type_seed,(seed_position1,seed_position2),cmap=cmap,iteration=iteration,speed=speed,save=save)

if __name__ == "__main__":
    main()


#print(main())
