from game_life_simulate import *
from generation import *
from generate_universe import *
from create_seed import *
from add_seed_to_universe import *
import matplotlib.pyplot as plt
import matplotlib.animation as anim

def simple_animation(universe,cmap,iteration,speed):
    #montre les évolution de l'univers avec une frequence et un temps d'arret choisis
    fig=plt.figure()
    im=plt.imshow(universe,cmap=cmap,animated=True)

    def updatefig(*args):
        universe=im.get_array()
        im.set_array(generation(universe))
        return im,

    if iteration==-1:
        ani=anim.FuncAnimation(fig,updatefig,frames=500,interval=speed,blit=True)
    else:
        ani=anim.FuncAnimation(fig,updatefig,frames=iteration,interval=speed,blit=True,repeat=False)

    plt.show()

#universe=generate_universe((6,6))
#seed=create_seed("beacon")
#universe=add_seed_to_universe(seed,universe,1,1)
#print(simple_animation(universe,'Greys',10,500))
#print(seed)
#print(universe)
#print(survival(universe,4,4))

