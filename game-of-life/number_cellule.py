from cellular_shape import *

def number_cellule(im):
    seed=np.array(im)
    shape=cellular_shape(im)
    n,p,r=np.shape(seed)
    m=0
    q=0
    x=0
    y=0
    while ((seed[x][y][0:3]!=np.array([0,0,0])).all() or (seed[x][y][0:3]!=np.array([255,255,255])).all()) and x<=n-shape and y<=p-shape:
        if x<n-shape:
            x+=1
        else:
            x=0
            y+=1
    while ((seed[x][y][0:3]==np.array([0,0,0])).all() and (seed[x][y][0:3]!=np.array([255,255,255])).all())and x<=n-shape and y<=p-shape:
        if x<n-shape:
            x+=shape
            m+=1
        else:
            y+=shape
            q+=1
    while ((seed[x][y][0:3]!=np.array([0,0,0])).all() and (seed[x][y][0:3]==np.array([255,255,255])).all())and x<=n-shape and y<=p-shape:
        if x<n-shape:
            x+=shape
            m+=1
        else:
            y+=shape
            q+=1
    return (m,q)

#print(number_cellule(im))



