from  game_life_simulate import *
from pytest import *

def test_game_life_simulate():
    beacon_1=[[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]
    beacon_2=[[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,0,0,0,0],[0,0,0,0,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]
    assert (beacon_2==game_life_simulate(beacon_1,3)).all()
    assert (beacon_1==game_life_simulate(beacon_1,4)).all()

test_game_life_simulate()
