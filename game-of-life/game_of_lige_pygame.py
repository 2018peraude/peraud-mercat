import pygame as pg
import random as rd
import numpy as np
from generate_universe import *
from create_seed import *
from add_seed_to_universe import *
from generation import *
from get_color import *

def pygame_simul(size1,size2,type_seed,seed_position1,seed_position2,couleur_vivante,couleur_morte,fond):


        #fait évoluer l'univers
        def update(universe):
                universe2=generation(universe)
                for i in range(size1):
                        for j in range(size2):
                                universe[i][j]=universe2[i][j]

        #main game function
        def play():
                #initialization
                pg.init()
                scrn = pg.display.set_mode((500, 500))
                mainsrf = pg.Surface((500, 500))
                mainsrf.fill(get_color(fond))
                print("Generating")
                empty_universe=generate_universe((size1,size2))
                seed=create_seed(type_seed)
                universe=add_seed_to_universe(seed,empty_universe,seed_position1,seed_position2)
                print("Generating done")
                #game cycle
                while 1:
                        #tracking quitting
                        for event in pg.event.get():
                                if event.type == pg.QUIT:
                                        pg.quit()
                                        sys.exit()
                        #drawing
                        for y in range(size2):
                                for x in range(size1):
                                        if universe[x][y]==1:
                                                pg.draw.rect(mainsrf, get_color(couleur_vivante), (x*10, y*10, 10, 10))
                                        else:
                                                pg.draw.rect(mainsrf, get_color(couleur_morte), (x*10, y*10, 10, 10))
                        update(universe)
                        scrn.blit(mainsrf, (0, 0))
                        pg.display.update()


        play()

#pygame_simul(6,6,'blinker',1,1)
