import numpy as np
from create_seed import *
from generate_universe import *

def add_seed_to_universe(seed,universe,x_start,y_start):
    #permet d'ajouter une graine choisie à l'univers
    x,y=np.shape(universe)  #prend en compte la taille de l'univers
    n,p=np.shape(seed)      #prend en compte la taille de la graine
    for i in range(n):
        for j in range(p):
            universe[(x_start+i)%x][(y_start+j)%y]=seed[i][j]
    return universe

#seed=create_seed("r_pentomino")
#universe=generate_universe((6,6))
#print(add_seed_to_universe(seed,universe,1,1))
#universe=generate_universe((6,6))
#print(add_seed_to_universe(seed,universe,2,2))


