import numpy as np
import matplotlib.pyplot as plt
import imageio as img

im=img.imread('D:\\Bureau\\162px-Game_of_life_infinite1.svg.png')
seed=np.array(im)

def cellular_shape(im):
    #recupere a taille des cellules d'une graine sous format image
    pixel_seed=np.array(im)
    n,p,q=np.shape(pixel_seed)
    shape=0
    x=0
    y=0
    while (pixel_seed[x][y][0:3]!=np.array([0,0,0])).all() and x<=n and y<=p:
        if x<n-1:
            x+=1
        else:
            y+=1
            x=0
    if x==n and y==p:
        return 'Not a good seed'
    else:
        while (pixel_seed[x][y][0:3]==np.array([0,0,0])).all():
            shape+=1
            x+=1
            y+=1
    return shape

#print(cellular_shape(im))



