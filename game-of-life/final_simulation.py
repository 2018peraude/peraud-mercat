from game_of_lige_pygame import *
from tkinter import *
import tkinter.ttk as ttk
import random as rd

def lancer_fenetre():
    def lancer_simu():
        size1=int(var_hauteur.get())
        size2=int(var_largeur.get())
        type_seed=graine.get()
        seed_position2=int(var_ordonnee.get())
        seed_position1=int(var_abscisse.get())
        couleur_vivante=couleur1.get()
        couleur_morte=couleur2.get()
        fond=couleur3.get()
        if seed_position1==-1:
            seed_position1=rd.randint(0,size1)
        if seed_position2==-1:
            seed_position2=rd.randint(0,size2)
        pygame_simul(size1,size2,type_seed,seed_position1,seed_position2,couleur_vivante,couleur_morte,fond)
    # On crée une fenêtre, racine de notre interface
    fenetre = Tk()

    # Label
    champ_label = Label(fenetre, text="Rentrez les paramètres d'initialisation du jeu",font= ('Times', '24', 'bold'))
    champ_label.pack()

    frame=Frame(fenetre)
    frame.pack()

    #taille de la grille

    var_largeur=StringVar(frame)
    text_largeur= Label(frame, text= "largeur de la grille",font=('Times','16'))
    text_largeur.grid(row=0,column=0)
    largeur = Entry (frame,textvariable=var_largeur)
    largeur.grid(row=0,column=2)

    var_hauteur=StringVar(frame)
    text_hauteur= Label(frame, text= "hauteur de la grille",font=('Times','16'))
    text_hauteur.grid(row=1,column=0)
    hauteur = Entry (frame,textvariable=var_hauteur)
    hauteur.grid(row=1,column=2)

    #type de graine
    text_graine= Label(frame, text= "type de graine",font=('Times','16'))
    text_graine.grid(row=2,column=0)
    graine = ttk.Combobox(frame,values=["boat","r_pentomino","beacon","acorn","block_switch_engine","infinite","glider","pentadecathlon","blinker","toad"])
    graine.grid(row=2,column=2)

    #Place de la graine

    var_abscisse=StringVar(frame)
    text_abscisse= Label(frame, text= "abscisse de la graine (-1 pour aléatoire)",font=('Times','16'))
    text_abscisse.grid(row=3,column=0)
    abscisse = Entry (frame,textvariable=var_abscisse)
    abscisse.grid(row=3,column=2)
    var_abscisse.set("-1")

    var_ordonnee=StringVar(frame)
    text_ordonnee= Label(frame, text= "ordonnee de la graine (-1 pour aléatoire)",font=('Times','16'))
    text_ordonnee.grid(row=4,column=0)
    ordonnee = Entry (frame,textvariable=var_ordonnee)
    ordonnee.grid(row=4,column=2)
    var_ordonnee.set("-1")

    #nombre d'itérations
    var_iterations=StringVar(frame)
    text_iterations= Label(frame, text= "nombre d'itérations (-1 pour un infinité)",font=('Times','16'))
    text_iterations.grid(row=5,column=0)
    iterations = Entry (frame,textvariable=var_iterations)
    iterations.grid(row=5,column=2)
    var_iterations.set("-1")

    #cmap
    text_couleur1= Label(frame, text= "couleur des cellules vivantes",font=('Times','16'))
    text_couleur1.grid(row=6,column=0)
    couleur1 = ttk.Combobox(frame,values=["Black","White","Blue","Green","Red","Purple"])
    couleur1.grid(row=6,column=2)
    couleur1.current(0)

    text_couleur2= Label(frame, text= "couleur des cellules mortes",font=('Times','16'))
    text_couleur2.grid(row=7,column=0)
    couleur2 = ttk.Combobox(frame,values=["Black","White","Blue","Green","Red","Purple"])
    couleur2.grid(row=7,column=2)
    couleur2.current(0)

    text_couleur3= Label(frame, text= "couleur du fond",font=('Times','16'))
    text_couleur3.grid(row=8,column=0)
    couleur3 = ttk.Combobox(frame,values=["Black","White","Blue","Green","Red","Purple"])
    couleur3.grid(row=8,column=2)
    couleur3.current(0)


    # Sauvegarder
    var_case = BooleanVar()
    case = Checkbutton(fenetre, text="Sauvegarder", variable=var_case,font=('Times','16'))
    case.pack()

    #Bouton Lancer
    bouton_lancer=Button(fenetre,text="Lancer la simulation",command=lancer_simu,font=('Times','16'))
    bouton_lancer.pack()

    # On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
    fenetre.mainloop()

#lancer l'animation
if __name__=="__main__":
        lancer_fenetre()
