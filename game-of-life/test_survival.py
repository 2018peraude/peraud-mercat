from survival import *
from pytest import *
import numpy as np

universe=[[0,0,0,0,0],[0,1,1,1,0],[0,0,1,0,0],[0,0,0,0,0]]

def test_survival():
    assert survival(universe,1,1)==universe
    assert survival(universe,1,2)==universe
    assert survival(universe,2,3)==[[0,0,0,0,0],[0,1,1,1,0],[0,0,1,1,0],[0,0,0,0,0]]
    assert survival(universe,4,4)==universe
    assert survival([[1,1],[0,0],0,0])==[[0,1],[0,0]]
