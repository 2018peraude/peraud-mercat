from cellular_shape import *

def cellular_value(pixel_seed,x,y):
    if (pixel_seed[x][y][0:3]==np.array([0,0,0])).all():
        return 0
    elif (pixel_seed[x][y][0:3]==np.array([255,255,255])).all():
        return 1

